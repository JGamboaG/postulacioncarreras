#Configuración ambiente de desarrollo de IC.#

DESCRIPCIÓN:

-Configurar una plataforma de ​ Integración Continua ​ (IC).
-Implementar una aplicación y usarla como parte de la Plataforma de IC.

​APP:

-Debe ser construida usando ​ Spring y VueJS . 
-Utilizar Nginx para VueJs (frontend)
-Backend usar el Tomcat interno de Spring Boot.
-Para ambos casos se sugiere usar Docke.
-Usar ​ MySQL ​ como base de datos.
-Usar IDE de desarrollo (​Visual Studio Code, Eclipse, Netbeans o IntelliJ).

FUNCIONALIDAD:

a) Ingresar.

b) Listar por pantalla datos de alumnos postulantes a carreras de ingenierías
 de la USACh.

Los únicos datos que se requieren son:

-RUT del postulante.
-Nombre del postulante.
-Fecha nacimiento.
-Carrera a la que postula (elegir desde una lista de carreras).

IMPORTANTE: La Plataforma de IC (luego de correr todo el pipeline definido
en ​ Gitlab CI ) ​ debe desplegar en forma automática en el servidor Tomcat la ​ App
de tal manera que quede lista para ser usada desde un navegador Web.

IMPORTANTE: Tomcat y MySQL deben estar instalados en servidores virtuales
externos. Se debe usar la tecnología de ​ Dockers ​ según corresponda.

PLATAFIC - Plataforma de IC: ​ 

Mostrar el funcionamiento de toda la plataforma de IC con la aplicación desarrollada.
Los siguientes componentes de laplataforma de IC deben interactuar y funcionar en forma adecuada:

-Control de Versiones (Git - Gitlab): ​ instalado y funcionando correctamente como parte de la Plataforma de IC.
-Servidor de Integración Continua (Gitlab CI): ​ configurado y funcionando correctamente como parte de la Plataforma de IC. Debe tener definido un pipeline ​ visual.
-Análisis estático de código (Sonarqube​): instalado y funcionando correctamente como parte de la Plataforma de IC.
-​Pruebas unitarias y de integración (JUnit​): instalado y funcionando correctamente como parte de la Plataforma de IC. Deben escribir tres casos de prueba unitarios.
-Pruebas de Aceptación (Usando algún Webdriver. Por ejemplo: ​ Selenium, Protractor, etc.), instalado y funcionando correctamente como parte de la Plataforma de IC.
Deben escribir tres casos de prueba de aceptación usando el webdriver.
-Gestión Pruebas de Aceptación (Testlink): instalado y funcionando correctamente. Debe tener tres casos de prueba definidos.
-Gestión de Incidencias (Gitlab Issue Tracker): configurado y funcionando correctamente. Debe tener registrado tres ejemplos de incidencias.

IMPORTANTE: Se debe usar la tecnología de ​ Dockers ​ para levantar la Plataforma d IC, incluido ​ Gitlab Runner.
IMPORTANTE: Toda la Plataforma de IC debe estar configurada en servidores virtuales externos.
